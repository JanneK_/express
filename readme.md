# Node Task

This project contains node Hello World

## Installation steps

1. Copy the repository
2. Install dependencies
3. Start the application

## Terminal commands

code block:
```sh
git clone https://gitlab.com/JanneK_/express
cd ./node-task
npm i
```

Start cmd: `npm run start`

## After the app is working

### /get/ commands:
```
/add/
/root/?param
/qadd/?params
```
## App should work!
![Bonk](https://cdn.pixabay.com/photo/2018/10/09/04/17/gorilla-3734067_960_720.jpg)

