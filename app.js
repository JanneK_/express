
const express = require('express')
const app = express()
const port = 3000


/**
 * This function adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a,b) => {
    return a+b;
}
/**
 * This function returns sqrt from positive numbers
 * @param {Number} a
 * @returns {Number}
 */
const posRoot = (a) =>{
    if( a > 0 ) {
        return Math.sqrt(a)
    }
    else{        
        return 0
    }
}

app.get('/add/', (req, res) => {
    const x = add(1,2)
    res.send(`Sum: ${x}`)
  })

app.get('/qadd', (req, res)=> {
const x = parseInt(req.query.a);
const y = parseInt(req.query.b);
console.log({isANaN: isNaN(x), IsBNaN: isNaN(y)})
if(isNaN(x) || isNaN(y)){
    res.send('One of the parameters is not a number')
}
else {
    const calc = add(x,y)
    res.send(`Sum of params: ${calc.toString()}`)
}
})

app.get('/root', (req, res)=> {
    const x = parseInt(req.query.a);    
    console.log({isANaN: isNaN(x)})
    if(isNaN(x)){
        res.send('param is negative value or not a number')
    }
    else {
        const calc = posRoot(x)
        res.send(`Root of param: ${calc.toString()}`)
    }
})

app.get('/', (req, res) => {
  res.send('cmd: /add/, /qadd/, /root/')
})

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})